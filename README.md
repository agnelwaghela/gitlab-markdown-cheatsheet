# Gitlab Flavored Markdown Cheatsheet

Just a simple markdown cheatsheet for the [Gitlab Flavored Markdown](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/markdown/markdown.md)

[**`PDF`**](Gitlab.Markdown.pdf) | [**`PSD`**](Gitlab.Markdown.psd)

[![Markdown Reference](GitlabMarkdown.png)](Gitlab.Markdown.pdf)



## Why

Because I'm always forgetting about those codes, and the spaces and all that stuff.


### Fonts and stuff

[Source Code Pro](https://github.com/adobe-fonts/source-code-pro)

[Source Sand Pro](https://github.com/adobe-fonts/source-sans-pro)

[GitLab Logos](https://about.gitlab.com/press/)

[Font Awesome](http://fortawesome.github.io/Font-Awesome/)
